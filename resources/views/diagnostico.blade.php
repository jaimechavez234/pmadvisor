@extends('base.base')

@section('activeHome') active @stop

@section('content')
<!-- bradcam_area  -->
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <h3>Servicios</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ bradcam_area  -->

<div class="case_details_area">
    <div class="container">
        <div class="border_bottom">
            <div class="row ">
                <div class="col-xl-12">
                    <div class="details_title">
                        <span>Diagnóstico Empresarial</span>
                        <h3>La solución al estado actual de tu empresa</h3>
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="case_thumb">
                        <img src="img/case/services-1.png" alt="Diagnóstico Empresarial">
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="details_main_wrap">
                        <p class="details_info">
                            Conocer la situación actual de la empresa nos permite la detección oportuna de problemas que nos impide el desarrollo de la organización, por lo que tenemos que actualizarnos con las mejores prácticas, que permitan un mayor control y optimización de los recursos con los que cuentan nuestros clientes.
                        </p>
                        <div class="single_details">
                            <ul class="list-group list-group-horizontal-sm">
                                <li class="list-group-item list-group-item-primary">Estrategias acorde a las necesidades del cliente</li>
                                <li class="list-group-item list-group-item-primary">Detectar áreas de oportunidad</li>
                                <li class="list-group-item list-group-item-primary">Optimizar costos y recursos</li>
                                <li class="list-group-item list-group-item-primary">Identificar y reducir los tiempos de espera</li>
                                <li class="list-group-item list-group-item-primary">Desarrollo y posicionamiento de marca</li>
                            </ul>
                        </div>
                        <p class="details_info">Análisis de procesos de negocio permite la optimización de las operaciones para obtener el crecimiento sustentable de la organización</p>
                        <div class="single_details">
                            <span>Resumen Diagnóstico Empresarial</span>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Análisis de situación financiera</li>
                                <li class="list-group-item">Identidad corporativa</li>
                                <li class="list-group-item">Filosofía y Mercadotecnia Empresarial</li>
                                <li class="list-group-item">Detección de problemas</li>
                                <li class="list-group-item">Cobranza y recuperación de cartera vencida</li>
                                <li class="list-group-item">Organigrama y comunicación empresarial</li>
                                <li class="list-group-item">Análisis FODA</li>
                            </ul>
                        </div>
                        <div class="single_details mb-60">
                            <ul>
                                <li>
                                    <a href="/contacto"> <i class="fa fa-comment"> </i> Contactanos </a>
                                </li>
                            </ul>

                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Information_area  -->
<div class="Information_area overlay">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-xl-8">
                <div class="info_text text-center">
                    <h3>Para dudas o aclaraciones puede contactarnos.</h3>
                    <p>Nuestros especialistas estarán encantados de atender su solicitud.</p>
                    <a class="boxed-btn3" href="https://wa.me/5540854926/?text=Me%20gustaria%20recibir%20más%20información" target="_blank">+52 1 55 4085 4926</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Information_area  end -->
@stop