@extends('base.base')

@section('activeHome') active @stop

@section('content')

<!-- bradcam_area  -->
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <h3>Nosotros</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ bradcam_area  -->

<!-- about_info_area start  -->
<div class="about_info_area plus_padding">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-6 col-lg-6">
                <div class="about_text">
                    <h3>¿Quiénes somos?</h3>
                    <p>Somos una firma de consultores enfocados a ofrecer soluciones innovadoras y de valor para todos los sectores, nuestra fortaleza es el diagnóstico empresarial, gestión de proyectos, reingeniería de procesos de negocio, soluciones tecnológicas y capital humano.</p>
                    <a href="/contacto" class="boxed-btn3">Contáctanos</a>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="about_thumb">
                    <img src="img/service/about.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /about_info_area end  -->

<!-- counter_area  -->
<div class="counter_area counter_bg_1 overlay_03">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3">
                <div class="single_counter text-center">
                    <div class="counter_icon">
                        <img src="img/svg_icon/group.svg" alt="">
                    </div>
                    <h3> <span class="counter">200</span> <span> +</span> </h3>
                    <p>Miembros de equipo</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3">
                <div class="single_counter text-center">
                    <div class="counter_icon">
                        <img src="img/svg_icon/cart.svg" alt="">
                    </div>
                    <h3> <span class="counter">97</span> <span>%</span> </h3>
                    <p>Negocios exitosos</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3">
                <div class="single_counter text-center">
                    <div class="counter_icon">
                        <img src="img/svg_icon/heart.svg" alt="">
                    </div>
                    <h3> <span class="counter">5628</span></h3>
                    <p>Clientes felices</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3">
                <div class="single_counter text-center">
                    <div class="counter_icon">
                        <img src="img/svg_icon/respect.svg" alt="">
                    </div>
                    <h3> <span class="counter">5637</span> </h3>
                    <p>Trabajos terminados</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /counter_area  -->

<!-- about_info_area start  -->
<div class="about_info_area plus_padding">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-6 col-lg-6">
                <div class="about_thumb">
                    <img src="img/service/about.png" alt="">
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="about_text">
                    <h3>Nuestra misión</h3>
                    <p>Nuestra experiencia y compromiso nos permite desarrollar e implantar soluciones innovadoras que contribuyen al desarrollo de nuestros clientes; con la calidad esperada en los objetivos empresariales, estableciendo un alto grado de confianza y compromiso.</p>
                    <a href="/contacto" class="boxed-btn3">Contactanos</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /about_info_area end  -->

<!-- about_info_area start  -->
<div class="about_info_area plus_padding">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-6 col-lg-6">
                <div class="about_text">
                    <h3>Nuestra visión</h3>
                    <p>Ser los principales socios de negocio de las empresas líderes en México que buscan fortalecerse y ser un referente, desarrollando su crecimiento y productividad Trabajamos asesorando a los líderes de la industria en servicios: Financieros; Digitales; Productos de Consumo; Energía; Cuidado de la Salud y Sector Público.</p>
                    <a href="/contacto" class="boxed-btn3">Contáctanos</a>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="about_thumb">
                    <img src="img/service/about.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /about_info_area end  -->

<!-- service_area_start -->
<div class="service_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center mb-50">
                    <h3>Que es lo que hacemos?</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/diagnostico.svg" width="40px" alt="Diagnóstico Empresarial">
                    </div>
                    <h3>Diagnóstico Empresarial</h3>
                    <p>Conocer la situación actual de la empresa nos permite la detección oportuna de problemas.</p>
                    <a href="/diagnosticoempresarial" class="learn_more">Leer más</a>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/idea.svg" width="40px" alt="Consultoría y administración de proyectos">
                    </div>
                    <h3>Consultoría y administración de proyectos</h3>
                    <p>Dirección de proyectos, de estructuras, planes y estrategias que nos permitan tener un control sobre: presupuestos, costos, tiempo, etc.</p>
                    <a href="/direcciondeproyectos" class="learn_more">Leer más</a>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/habilidad.svg" width="40px" alt="Reingenieria de procesos de negocio">
                    </div>
                    <h3>Reingenieria de procesos de negocio</h3>
                    <p>Acompañamos a nuestros clientes en el desarrollo de planes y estrategias corporativas.</p>
                    <a href="/reingenieriadeprocesos" class="learn_more">Leer más</a>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/cerebro.svg" width="40px" alt="Soluciones Tecnológicas">
                    </div>
                    <h3>Soluciones Tecnológicas</h3>
                    <p>Las mejores soluciones tecnologicas para implementar en tu empresa.</p>
                    <a href="/solucionestecnologicas" class="learn_more">Leer más</a>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/recursos-humanos.svg" width="40px" alt="Capital Humano">
                    </div>
                    <h3>Capital Humano</h3>
                    <p>Identificar las necesidades de nuestros colaboradores nos permite conocer las necesidades que nos impiden que realicen su trabajo satisfactoriamente.</p>
                    <a href="/capitalhumano" class="learn_more">Leer más</a>
                </div>
            </div>
            <!-- <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/6.svg" alt="">
                    </div>
                    <h3>Travel Agency </h3>
                    <p>Esteem spirit temper too say adieus who direct esteem.</p>
                    <a href="#" class="learn_more">Learn More</a>
                </div>
            </div> -->
        </div>
    </div>
</div>
<!-- service_area_end -->

@stop