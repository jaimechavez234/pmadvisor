<!doctype html>
<html class="no-js" lang="esp">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>PMAdvisor</title>

    <meta name="description" content="Somos una firma de consultores enfocados a ofrecer soluciones innovadoras y de valor para todos los sectores, nuestra fortaleza es la Dirección de Proyectos, reingeniería y análisis de procesos de negocio, soluciones tecnológicas y capital humano.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="pmadvisor, consultores, firma, soluciones, innovadores, Diagnóstico Empresarial, Consultoría y administración de proyectos, Reingenieria de procesos de negocio, Soluciones Tecnológicas, Capital Humano, colaboradores, pmadvisor.mx, PMADVISOR.MX, pm advisor, advisor pm, advisorpm, advisorpm.mx, consultores pmadvisor, pmadvisor consultores, pm advisor cnsultores, pm advisor consultor, pmadvisor consultor, consultor de negocios, consultores de negocios, pm advisor consultor de negocio, pm advisor consultores de negocio, negocios" />
    <meta name="author" content="PMAdvisor" />
    <meta name="copyright" content="2020, PMAdvisor" />
    <LINK REV="made" href="mailto:info@pmadvisor.mx">
    <META NAME="Resource-type" CONTENT="Homepage">
    <META NAME="Revisit-after" CONTENT="5 days">
    <META NAME="Revisit-after" CONTENT="5 days">

    <meta name="robots" content="index" />
    <meta name="robots" content="follow" />
    <META NAME="robots" content="ALL">

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="img/pm.ico">
    <!-- Place favicon.ico in the root directory -->
    <html prefix="og: http://ogp.me/ns#">

    <link rel="profile" href="https://gmpg.org/xfn/11">
    <meta property="og:locale" content="es_ES">
    <meta name="twitter:card" content="summary_large_image">
    <meta property="og:type" content="website" />
    <meta property=og:title content="PMadvisor-Web Site" />
    <meta property="og:description" content="Somos una firma de consultores enfocados a ofrecer soluciones innovadoras y de valor para todos los sectores, nuestra fortaleza es la Dirección de Proyectos, reingeniería y análisis de procesos de negocio, soluciones tecnológicas y capital humano." />
    <meta property="og:image:url" content="http://pmadvisor.mx/img/fav.png" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="200">
    <meta property="og:image:height" content="200">
    <meta property="og:url" content="http://pmadvisor.mx/" />
    <meta property="og:image:secure_url" content="https://pmadvisor.mx/img/fav.png" />
    <meta property="og:site_name" content="PMadvisor" />
    <meta property="article:modified_time" content="2020-04-23T16:55:50+00:00">

    <!-- CSS here -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/gijgo.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slicknav.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- <link rel="stylesheet" href="css/responsive.css"> -->
</head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v7.0&appId=532142227609973&autoLogAppEvents=1" nonce="iaEAeTYe"></script>

    @include('base.header')

    @yield('content')

    @include('base.footer')

    <!-- link that opens popup -->


    <!-- JS here -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/ajax-form.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/scrollIt.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/nice-select.min.js"></script>
    <script src="js/jquery.slicknav.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/gijgo.min.js"></script>
    <script src="js/slick.min.js"></script>
    <!--contact js-->
    <script src="js/contact.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>

    <script src="js/main.js"></script>
</body>

</html>