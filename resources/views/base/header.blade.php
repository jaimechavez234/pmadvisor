<!-- header-start -->
<header>
    <div class="header-area ">
        <div class="header-top_area d-none d-lg-block">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-md-5 ">
                        <div class="header_left">
                            <p>Bienvenido a PMAdvisor</p>
                        </div>
                    </div>
                    <div class="col-xl-7 col-md-7">
                        <div class="header_right d-flex">
                            <div class="short_contact_list">
                                <ul>
                                    <li><a href="#"> <i class="fa fa-envelope"></i> info@pmadvisor.mx</a></li>
                                    <li><a href="#"> <i class="fa fa-phone"></i> +52 1 55 4085 4926</a></li>
                                </ul>
                            </div>
                            <div class="social_media_links">
                                <!-- <a href="#">
                                    <i class="fa fa-linkedin"></i>
                                </a> -->
                                <a href="https://www.facebook.com/PMAdvisorSAdeCV/" target="_blank" rel="Red social de pmadvisor">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <!-- <a href="#">
                                    <i class="fa fa-instagram"></i>
                                </a> -->
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div id="sticky-header" class="main-header-area">
            <div class="container">
                <div class="header_bottom_border">
                    <div class="row align-items-center">
                        <div class="col-xl-3 col-lg-2">
                            <div class="logo">
                                <a href="/">
                                    <img src="img/logo.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-7">
                            <div class="main-menu  d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a class="{{ ! Route::is('index') ?: 'active' }}" href="/">Inicio</a></li>
                                        <li><a class="{{ ! Route::is('servicios') ?: 'active' }}" href="/servicios">Servicios <i class="ti-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a class="{{ ! Route::is('servicios') ?: 'active' }}" href="/diagnosticoempresarial">Diagnóstico Empresarial</a></li>
                                                <li><a class="{{ ! Route::is('servicios') ?: 'active' }}" href="/direcciondeproyectos">Consultoría y administración de proyectos</a></li>
                                                <li><a class="{{ ! Route::is('servicios') ?: 'active' }}" href="/reingenieriadeprocesos">Reingenieria de procesos de negocio</a></li>
                                                <li><a class="{{ ! Route::is('servicios') ?: 'active' }}" href="/solucionestecnologicas">Soluciones Tecnologicas</a></li>
                                                <li><a class="{{ ! Route::is('servicios') ?: 'active' }}" href="/capitalhumano">Capital Humano</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="{{ ! Route::is('nosotros') ?: 'active' }}" href="/nosotros">Nosotros</a></li>
                                        <li><a class="{{ ! Route::is('contacto') ?: 'active' }}" href="/contacto">Contacto</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                            <div class="Appointment">
                                <div class="book_btn d-none d-lg-block">
                                    <a href="/contacto">Solicitar información</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</header>
<!-- header-end -->