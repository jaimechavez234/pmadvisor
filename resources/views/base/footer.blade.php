<!-- footer start -->
<footer class="footer">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-md-6 col-lg-3">
                    <div class="footer_widget">
                        <div class="footer_logo">
                            <a href="/">
                                <img src="img/logo.png" alt="">
                            </a>
                        </div>
                        <p>
                            <a href="mailto:info@pmadvisor.mx">info@pmadvisor.mx</a> <br>
                            +52 1 55 4085 4926 <br>
                            Cerro Libertad 233, Campestre Churubusco, Coyoacán, 04200 Ciudad de México, CDMX
                        </p>
                        <div class="socail_links">
                            <ul>
                                <!-- <li>
                                    <a href="#">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li> -->
                                <li>
                                    <a href="https://www.facebook.com/PMAdvisorSAdeCV/" target="_blank" rel="Red social de pmadvisor">
                                        <i class="ti-facebook"></i>
                                    </a>
                                </li>
                                <!-- <li>
                                    <a href="#">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li> -->
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-xl-2 col-md-6 col-lg-3">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                            Servicios
                        </h3>
                        <ul>
                            <li><a class="{{ ! Route::is('servicios') ?: 'active' }}" href="/diagnosticoempresarial">Diagnóstico Empresarial</a></li>
                            <li><a class="{{ ! Route::is('servicios') ?: 'active' }}" href="/direcciondeproyectos">Consultoría y administración de proyectos</a></li>
                            <li><a class="{{ ! Route::is('servicios') ?: 'active' }}" href="/reingenieriadeprocesos">Reingenieria de procesos de negocio</a></li>
                            <li><a class="{{ ! Route::is('servicios') ?: 'active' }}" href="/solucionestecnologicas">Soluciones Tecnologicas</a></li>
                            <li><a class="{{ ! Route::is('servicios') ?: 'active' }}" href="/capitalhumano">Capital Humano</a></li>
                        </ul>

                    </div>
                </div>
                <div class="col-xl-2 col-md-6 col-lg-2">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                            Links
                        </h3>
                        <ul>
                            <li><a class="{{ ! Route::is('index') ?: 'active' }}" href="/">Inicio</a></li>
                            <li><a class="{{ ! Route::is('servicios') ?: 'active' }}" href="/servicios">Servicios</a></li>
                            <li><a class="{{ ! Route::is('nosotros') ?: 'active' }}" href="/nosotros">Nosotros</a></li>
                            <li><a class="{{ ! Route::is('contacto') ?: 'active' }}" href="/contacto">Contacto</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                            Recibe notias de nostros
                        </h3>
                        <form action="{{route('suscribete')}}" method="POST" class="newsletter_form">
                            @csrf
                            <input type="email" name="email" placeholder="Ingrese su email">
                            <button type="submit">Suscribete</button>
                        </form>
                        <p class="newsletter_text">Al suscribirte a nuestro boletín, aceptas recibir noificaciones de nuestras promociones o noticias de nuestros servicios..</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-xl-12">
                    <p class="copy_right text-center">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                        </script> All rights reserved PMAdvisor| Esta página esta hecha con <i class="fa fa-heart-o" aria-hidden="true"></i> por <a href="https://hjalter.com/" target="_blank">Hjalter Consultores</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/ footer end  -->