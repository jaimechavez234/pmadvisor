@extends('base.base')

@section('activeHome') active @stop

@section('content')
<!-- bradcam_area  -->
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <h3>Servicios</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ bradcam_area  -->

<div class="case_details_area">
    <div class="container">
        <div class="border_bottom">
            <div class="row ">
                <div class="col-xl-12">
                    <div class="details_title">
                        <span>Soluciones Tecnologicas</span>
                        <h3>Implementa tecnología de vanguardia en tu empresa.</h3>
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="case_thumb">
                        <img src="img/case/services-2.png" alt="Soluciones Tecnologicas">
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="details_main_wrap">
                        <p class="details_info">
                            Estamos enfocamos en ofrecer un servicio integral para cubrir las necesidades de nuestros clientes, utilizando un enfoque innovador que permita aprovechar los avances tecnológicos y convertir las oportunidades en fortaleza para la operación.
                        </p>
                        <div class="single_details">
                            <ul class="list-group list-group-horizontal-sm">
                                <li class="list-group-item list-group-item-primary">Desarrollos a la medida</li>
                                <li class="list-group-item list-group-item-primary">Hardware</li>
                                <li class="list-group-item list-group-item-primary">HelpDesk</li>
                                <li class="list-group-item list-group-item-primary">Hosting</li>
                                <li class="list-group-item list-group-item-primary">Software</li>
                                <li class="list-group-item list-group-item-primary">Soporte Técnico</li>
                            </ul>
                        </div>
                        <p class="details_info">Nuestro principal objetivo es hacer que nuestros clientes usen la tecnología a su favor para potencializar la información y se dediquen a operar su negocio con tranquilidad..</p>
                        <div class="single_details">
                            <span>Más servicios...</span>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Servicio HelpDesk (Iguala, Medio Tiempo, Tiempo Completo, Área Completa).</li>
                                <li class="list-group-item">Consultoría de Stack Tecnológico para operar modelos de negocio escalables (Nivel Internacional).</li>
                                <li class="list-group-item">Re-Infraestructura de Área de Tecnologías de la Información.</li>
                                <li class="list-group-item">Implementación y Soporte (Aspel, GSuite, Adobe).</li>
                            </ul>
                        </div>
                        <div class="single_details mb-60">
                            <ul>
                                <li>
                                    <a href="/contacto"> <i class="fa fa-comment"> </i> Contactanos </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Information_area  -->
<div class="Information_area overlay">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-xl-8">
                <div class="info_text text-center">
                    <h3>Para dudas o aclaraciones puede contactarnos.</h3>
                    <p>Nuestros especialistas estarán encantados de atender su solicitud.</p>
                    <a class="boxed-btn3" href="https://wa.me/5540854926/?text=Me%20gustaria%20recibir%20más%20información" target="_blank">+52 1 55 4085 4926</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Information_area  end -->

@stop