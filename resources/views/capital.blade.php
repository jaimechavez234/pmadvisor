@extends('base.base')

@section('activeHome') active @stop

@section('content')

<!-- bradcam_area  -->
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <h3>Servicios</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ bradcam_area  -->

<div class="case_details_area">
    <div class="container">
        <div class="border_bottom">
            <div class="row ">
                <div class="col-xl-12">
                    <div class="details_title">
                        <span>Capital Humano</span>
                        <h3>Selección, reclutamiento y motivación personal para los colaboradores de tu empresa.</h3>
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="case_thumb">
                        <img src="img/case/services-4.png" alt="Capital Humano">
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="details_main_wrap">
                        <p class="details_info">
                            Identificar las necesidades de nuestros colaboradores nos permite conocer las necesidades que nos impiden que realicen su trabajo satisfactoriamente y la rotación del personal, costando tiempo y dinero para nuestros clientes, por lo que contar con la correcta capacitación y motivación nos ayudará a reducir desperdicios:
                        </p>
                        <div class="single_details">
                            <ul class="list-group list-group-horizontal-sm">
                                <li class="list-group-item list-group-item-primary">Motivación de personal</li>
                                <li class="list-group-item list-group-item-primary">Clima organizacional óptimo para laborar</li>
                                <li class="list-group-item list-group-item-primary">Desarrollo y crecimiento de personal</li>
                                <li class="list-group-item list-group-item-primary">Incentivos no económicos</li>
                            </ul>
                        </div>
                        <p class="details_info">La pieza más importante de toda organización se centra en nuestros colaboradores, a ellos se debe el éxito o fracaso de toda empresa, así como las decisiones oportunas que nos ayudan a conducirla hacia el resultado esperado Es importante que conozcan la filosofía empresarial y se encuentre en un ambiente óptimo para la realización de las tareas y actividades diarias .</p>
                        <div class="single_details">
                            <span>Resumen Reingenieria de procesos de negocio</span>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Selección y reclutamiento de personal</li>
                                <li class="list-group-item">Capacitación</li>
                                <li class="list-group-item">Motivación de personal</li>
                            </ul>
                        </div>
                        <div class="single_details mb-60">
                            <ul>
                                <li>
                                    <a href="/contacto"> <i class="fa fa-comment"> </i> Contactanos </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Information_area  -->
<div class="Information_area overlay">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-xl-8">
                <div class="info_text text-center">
                    <h3>Para dudas o aclaraciones puede contactarnos.</h3>
                    <p>Nuestros especialistas estarán encantados de atender su solicitud.</p>
                    <a class="boxed-btn3" href="https://wa.me/5540854926/?text=Me%20gustaria%20recibir%20más%20información" target="_blank">+52 1 55 4085 4926</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Information_area  end -->

@stop