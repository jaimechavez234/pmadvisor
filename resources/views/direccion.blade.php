@extends('base.base')

@section('activeHome') active @stop

@section('content')
<!-- bradcam_area  -->
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <h3>Servicios</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ bradcam_area  -->

<div class="case_details_area">
    <div class="container">
        <div class="border_bottom">
            <div class="row ">
                <div class="col-xl-12">
                    <div class="details_title">
                        <span>Consultoría y administración de proyectos</span>
                        <h3>La solución al estado actual de los proyectos en tu empresa</h3>
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="case_thumb">
                        <img src="img/case/service-2.png" alt="Consultoría y administración de proyectos">
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="details_main_wrap">
                        <p class="details_info">
                            Actualmente las organizaciones se encuentran en constante movimiento en un mercado cambiario, lo cual nos lleva a la necesidad de creación de proyectos, de estructuras, planes y estrategias que nos permitan tener un control sobre: presupuestos, costos, tiempo, etc Para mitigar riesgos en nuestros clientes y obtengan los resultados deseados como:
                        </p>
                        <div class="single_details">
                            <ul class="list-group list-group-horizontal-sm">
                                <li class="list-group-item list-group-item-primary">Máximo aprovechamiento de los recursos</li>
                                <li class="list-group-item list-group-item-primary">Alcanzar objetivos en tiempo estimado</li>
                                <li class="list-group-item list-group-item-primary">Control de riesgos</li>
                                <li class="list-group-item list-group-item-primary">Gestión de costos y plazos</li>
                                <li class="list-group-item list-group-item-primary">Utilización de las mejores prácticas avaladas por el PMI</li>
                            </ul>
                        </div>
                        <p class="details_info">Consultoria y administracion de proyectos contamos con certificaciones que nos avalan como profesionales nos permiten entender la necesidad que requieren los clientes para darles las herramientas necesarias para alcanzar el éxito deseado.</p>
                        <div class="single_details">
                            <span>Resumen Consultoría y administración de proyectos</span>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Análisis y diseño detallado de requerimientos</li>
                                <li class="list-group-item">Implementación de proyectos – Lean, SixSigma</li>
                                <li class="list-group-item">Aplicación de metodologías tradicionales y ágiles</li>
                                <li class="list-group-item">Integración de Soluciones</li>
                                <li class="list-group-item">Mesa de servicio</li>
                                <li class="list-group-item">Implementación de PMO</li>
                                <li class="list-group-item">Capacitación</li>
                                <li class="list-group-item">Agile & Waterfall</li>
                            </ul>
                        </div>
                        <div class="single_details mb-60">
                            <ul>
                                <li>
                                    <a href="/contacto"> <i class="fa fa-comment"> </i> Contactanos </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Information_area  -->
<div class="Information_area overlay">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-xl-8">
                <div class="info_text text-center">
                    <h3>Para dudas o aclaraciones puede contactarnos.</h3>
                    <p>Nuestros especialistas estarán encantados de atender su solicitud.</p>
                    <a class="boxed-btn3" href="https://wa.me/5540854926/?text=Me%20gustaria%20recibir%20más%20información" target="_blank">+52 1 55 4085 4926</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Information_area  end -->

@stop