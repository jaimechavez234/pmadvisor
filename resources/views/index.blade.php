@extends('base.base')

@section('activeHome') active @stop

@section('content')

<!-- slider_area_start -->
<div class="slider_area">
    <div class="slider_active owl-carousel">
        <div class="single_slider  d-flex align-items-center slider_bg_1 overlay2">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="slider_text ">
                            <h3>Gran oportunidad <br>
                                para tu negocio.</h3>
                            <div class="video_service_btn">
                                <a href="/servicios" class="boxed-btn3">Nuestros servicios</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="single_slider  d-flex align-items-center slider_bg_2 overlay2">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="slider_text ">
                            <h3>Fortalece <br>
                                tu negocio</h3>
                            <div class="video_service_btn">
                                <a href="/servicios" class="boxed-btn3">Nuestros servicios</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="single_slider  d-flex align-items-center slider_bg_1 overlay2">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="slider_text ">
                            <h3>Desarrolla el crecimiento<br>
                                de tu negocio</h3>
                            <div class="video_service_btn">
                                <a href="/servicios" class="boxed-btn3">Nuestros servicios</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="single_slider  d-flex align-items-center slider_bg_2 overlay2">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="slider_text ">
                            <h3>Crece la productividad <br>
                                en tu negocio</h3>
                            <div class="video_service_btn">
                                <a href="/servicios" class="boxed-btn3">Nuestros servicios</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- slider_area_end -->

<!-- service_area_start -->
<div class="service_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center mb-50">
                    <h3>Que es lo que hacemos?</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/diagnostico.svg" width="40px" alt="Diagnóstico Empresarial">
                    </div>
                    <h3>Diagnóstico Empresarial</h3>
                    <p>Conocer la situación actual de la empresa nos permite la detección oportuna de problemas.</p>
                    <a href="/diagnosticoempresarial" class="learn_more">Leer más</a>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/idea.svg" width="40px" alt="Consultoría y administración de proyectos">
                    </div>
                    <h3>Consultoría y administración de proyectos</h3>
                    <p>Dirección de proyectos, de estructuras, planes y estrategias que nos permitan tener un control sobre: presupuestos, costos, tiempo, etc.</p>
                    <a href="/direcciondeproyectos" class="learn_more">Leer más</a>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/habilidad.svg" width="40px" alt="Reingenieria de procesos de negocio">
                    </div>
                    <h3>Reingenieria de procesos de negocio</h3>
                    <p>Acompañamos a nuestros clientes en el desarrollo de planes y estrategias corporativas.</p>
                    <a href="/reingenieriadeprocesos" class="learn_more">Leer más</a>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/cerebro.svg" width="40px" alt="Soluciones Tecnológicas">
                    </div>
                    <h3>Soluciones Tecnológicas</h3>
                    <p>Las mejores soluciones tecnologicas para implementar en tu empresa.</p>
                    <a href="#" class="learn_more">Leer más</a>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/recursos-humanos.svg" width="40px" alt="Capital Humano">
                    </div>
                    <h3>Capital Humano</h3>
                    <p>Identificar las necesidades de nuestros colaboradores nos permite conocer las necesidades que nos impiden que realicen su trabajo satisfactoriamente.</p>
                    <a href="/capitalhumano" class="learn_more">Leer más</a>
                </div>
            </div>
            <!-- <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/6.svg" alt="">
                    </div>
                    <h3>Travel Agency </h3>
                    <p>Esteem spirit temper too say adieus who direct esteem.</p>
                    <a href="#" class="learn_more">Learn More</a>
                </div>
            </div> -->
        </div>
    </div>
</div>
<!-- service_area_end -->

<!-- about_info_area start  -->
<div class="about_info_area">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-6 col-lg-6">
                <div class="about_text">
                    <h3>Por qué escoger PMAdvisor?</h3>
                    <p>Somos una firma de consultores enfocados a ofrecer soluciones innovadoras y de valor para todos los sectores, nuestra fortaleza es la Dirección de Proyectos, reingeniería y análisis de procesos de negocio, soluciones tecnológicas y capital humano.
                    </p>
                    <a href="/nosotros" class="boxed-btn3">Acerca de nosotros</a>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="about_thumb">
                    <img src="img/service/about.png" alt="Por qué escoger PMAdvisor">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /about_info_area end  -->

<!-- counter_area  -->
<div class="counter_area counter_bg_1 overlay_03">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3">
                <div class="single_counter text-center">
                    <div class="counter_icon">
                        <img src="img/svg_icon/group.svg" alt="pmadvisor team">
                    </div>
                    <h3> <span class="counter">200</span> <span> +</span> </h3>
                    <p>Miembros de equipo</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3">
                <div class="single_counter text-center">
                    <div class="counter_icon">
                        <img src="img/svg_icon/cart.svg" alt="Negocios exitosos">
                    </div>
                    <h3> <span class="counter">97</span> <span>%</span> </h3>
                    <p>Negocios exitosos</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3">
                <div class="single_counter text-center">
                    <div class="counter_icon">
                        <img src="img/svg_icon/heart.svg" alt="Clientes felices">
                    </div>
                    <h3> <span class="counter">5628</span></h3>
                    <p>Clientes felices</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3">
                <div class="single_counter text-center">
                    <div class="counter_icon">
                        <img src="img/svg_icon/respect.svg" alt="Trabajos terminados">
                    </div>
                    <h3> <span class="counter">5637</span> </h3>
                    <p>Trabajos terminados</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /counter_area  -->

<!-- case_study_area  -->
<div class="case_study_area">
    <div class="container">
        <div class="border_bottom">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title text-center mb-40">
                        <h3>Clientes</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="case_active owl-carousel">
                        <div class="single_case">
                            <div class="case_thumb">
                                <img src="img/case/client-1.png" alt="Scotiabank">
                            </div>
                            <div class="case_heading">
                                <span>Recursos Humanos</span>
                                <h3><a href="/contacto">Scotiabank</a></h3>
                            </div>
                        </div>
                        <div class="single_case">
                            <div class="case_thumb">
                                <img src="img/case/client-2.png" alt="Axcan">
                            </div>
                            <div class="case_heading">
                                <span>Solcuiones tecnológicas</span>
                                <h3><a href="/contacto">Axcan</a></h3>
                            </div>
                        </div>
                        <div class="single_case">
                            <div class="case_thumb">
                                <img src="img/case/client-3.png" alt="Icsi Coporativo">
                            </div>
                            <div class="case_heading">
                                <span>Recursos Humanos</span>
                                <h3><a href="/contacto">Icsi Coporativo</a></h3>
                            </div>
                        </div>
                        <div class="single_case">
                            <div class="case_thumb">
                                <img src="img/case/client-4.png" alt="GNP Seguros">
                            </div>
                            <div class="case_heading">
                                <span>Solcuiones tecnológicas</span>
                                <h3><a href="/contacto">GNP Seguros</a></h3>
                            </div>
                        </div>
                        <div class="single_case">
                            <div class="case_thumb">
                                <img src="img/case/client-5.png" alt="PEMEX">
                            </div>
                            <div class="case_heading">
                                <span>Solcuiones tecnológicas</span>
                                <h3><a href="/contacto">PEMEX</a></h3>
                            </div>
                        </div>
                        <div class="single_case">
                            <div class="case_thumb">
                                <img src="img/case/client-6.png" alt="ORTHEC">
                            </div>
                            <div class="case_heading">
                                <span>Solcuiones tecnológicas</span>
                                <h3><a href="/contacto">ORTHEC</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="more_close_btn text-center">
                        <a href="/contacto" class="boxed-btn3-line">Contactar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /case_study_area  -->

<!-- accordion  -->
<div class="accordion_area">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-6 col-lg-6">
                <div class="accordion_thumb">
                    <img src="img/banner/accordion.png" alt="Preguntas más frecuentes">
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="faq_ask">
                    <h3>Preguntas más frecuentes</h3>
                    <div id="accordion">
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Cual es el horario de <span>atención?</span>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion" style="">
                                <div class="card-body">Esteem spirit temper too say adieus who direct esteem esteems luckily or picture placing drawing.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        Cual es la dirección de la oficina?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                <div class="card-body">Esteem spirit temper too say adieus who direct esteem esteems luckily or picture placing drawing.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Números de telefono para contacto?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion" style="">
                                <div class="card-body">Esteem spirit temper too say adieus who direct esteem esteems luckily or picture placing drawing.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- accordion  -->

<div class="testimonial_area overlay ">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="testmonial_active owl-carousel">
                    <div class="single_carousel">
                        <div class="single_testmonial text-center">
                            <div class="quote">
                                <img src="img/svg_icon/quote.svg" alt="">
                            </div>
                            <p>Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor <br>
                                sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec sed neque. <br>
                                Fusce ac mattis nulla. Morbi eget ornare dui. </p>
                            <div class="testmonial_author">
                                <div class="thumb">
                                    <img src="img/case/testmonial.png" alt="">
                                </div>
                                <h3>Jesus</h3>
                                <span>Hjalter Consultores</span>
                            </div>
                        </div>
                    </div>
                    <div class="single_carousel">
                        <div class="single_testmonial text-center">
                            <div class="quote">
                                <img src="img/svg_icon/quote.svg" alt="">
                            </div>
                            <p>Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor <br>
                                sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec sed neque. <br>
                                Fusce ac mattis nulla. Morbi eget ornare dui. </p>
                            <div class="testmonial_author">
                                <div class="thumb">
                                    <img src="img/case/testmonial.png" alt="">
                                </div>
                                <h3>Jesus</h3>
                                <span>Hjalter Consultores</span>
                            </div>
                        </div>
                    </div>
                    <div class="single_carousel">
                        <div class="single_testmonial text-center">
                            <div class="quote">
                                <img src="img/svg_icon/quote.svg" alt="">
                            </div>
                            <p>Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor <br>
                                sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec sed neque. <br>
                                Fusce ac mattis nulla. Morbi eget ornare dui. </p>
                            <div class="testmonial_author">
                                <div class="thumb">
                                    <img src="img/case/testmonial.png" alt="">
                                </div>
                                <h3>Jesus</h3>
                                <span>Hjalter Consultores</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- team_area  -->
<div class="team_area">
    <div class="container">
        <div class="border_bottom">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title mb-40 text-center">
                        <h3>
                            Nuestro equipo
                        </h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6"></div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="single_team">
                        <div class="team_thumb">
                            <img src="img/team/1.png" alt="Juan Pablo Cervantes Drue">
                        </div>
                        <div class="team_info text-center">
                            <h3>Juan Pablo Cervantes Drue</h3>
                            <p>CEO</p>
                            <div class="social_link">
                                <ul>
                                    <li><a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li><a href="#">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    </li>
                                    <li><a href="#">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6"></div>
            </div>
        </div>
    </div>
</div>
<!-- /team_area  -->

<div class="contact_form_quote">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-lg-6">
                <div class="form_wrap">
                    <h3>Contáctanos</h3>
                    @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="alert-heading">Muchas gracias por enviar tu mensaje!</h4>
                        <p>Aww yeah, muy pronto uno de nuestros asesores se comunicará contigo para resolver cualquier duda o sugerencia.</p>
                        <hr>
                        <p class="mb-0">{{Session::get('message')}}.</p>
                    </div>
                    @endif
                    <form action="/" method="POST">
                        @csrf
                        <input name="name" type="text" placeholder="Nombre completo">
                        <input name="email" type="email" placeholder="Email">
                        <input name="phone" type="number" placeholder="Telefono">
                        <input name="location" type="text" placeholder="Ciudad y estado">
                        <textarea name="message" id="" cols="30" rows="10" placeholder="Escribe tu duda o sugerencia"></textarea>
                        <button type="submit" class="boxed-btn3">Enviar</button>
                    </form>
                </div>
            </div>
            <div class="col-xl-6 offset-xl-1 col-lg-6">
                <div class="contact_thumb">
                    <img src="img/banner/contact_thumb.png" alt="contato pmadvisor">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Information_area  -->
<div class="Information_area overlay">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-xl-8">
                <div class="info_text text-center">
                    <h3>Para má información, contáctanos.</h3>
                    <p>Nuestros teléfonos siempre están abiertos para nuestros clientes.</p>
                    <a class="boxed-btn3" href="#">+52 1 55 4085 4926</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Information_area  end -->
@stop