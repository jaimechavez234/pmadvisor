<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mensaje recibido</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm col-md col-lg col-xl">

            </div>
            <div class="col-sm col-md col-lg col-xl">
                <div class="jumbotron">
                    <h1 class="display-4">Recibiste un mensaje desde la página web!</h1>
                    <p class="lead">Acabas de recibir un mensaje desde la página web, te sugerimos comunicarte lo más pronto posible con el cliente. Esto demuestra que te preocupas por ellos y mantienes una buena relación.</p>
                    <hr class="my-4">
                    <h3>Mensaje de: <br><span class="badge badge-secondary">{{ $msg['name'] }}</span></h3>
                    <h3>Correo electronico: <br><span class="badge badge-secondary">{{ $msg['email'] }}</span></h3>
                    <h3>Telefono: <br><span class="badge badge-secondary">{{ $msg['phone'] }}</span></h3>
                    <h3>Ciudad y estado: <br><span class="badge badge-secondary">{{ $msg['location'] }}</span></h3>
                    <h3>Mensaje: <br><span class="badge badge-secondary">{{ $msg['message'] }}</span></h3>
                    <a class="btn btn-primary btn-lg" href="mailto:{{ $msg['email'] }}" role="button">Responder</a>
                </div>
            </div>
            <div class="col-sm col-md col-lg col-xl">

            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>

</html>