@extends('base.base')

@section('activeHome') active @stop

@section('content')

<!-- bradcam_area  -->
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <h3>Contactanos</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ bradcam_area  -->

<!-- ================ contact section start ================= -->
<section class="contact-section">
    <div class="container">
        @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="alert-heading">Muchas gracias por enviar tu mensaje!</h4>
            <p>Aww yeah, muy pronto uno de nuestros asesores se comunicará contigo para resolver cualquier duda o sugerencia.</p>
            <hr>
            <p class="mb-0">{{Session::get('message')}}.</p>
        </div>
        @endif
        <div class="mb-5 pb-4">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2238.386873477301!2d-99.13696226982287!3d19.347361307619664!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1fe2f0c831477%3A0x2e129b196d085e45!2sCerro%20Libertad%20233%2C%20Campestre%20Churubusco%2C%20Coyoac%C3%A1n%2C%2004200%20Ciudad%20de%20M%C3%A9xico%2C%20CDMX!5e0!3m2!1ses!2smx!4v1594322732287!5m2!1ses!2smx" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>


        <div class="row">
            <div class="col-12">
                <h2 class="contact-title">Solicita más información</h2>
            </div>
            <div class="col-lg-8">
                <form class="form-contact contact_form" method="POST" action="{{route('contacto')}}">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <textarea class="form-control w-100" name="message" id="message" cols="30" rows="9" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Escriba su mensaje'" placeholder=" Escriba su mensaje"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control valid" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Escriba su nombre completo'" placeholder="Nombre Completo">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control valid" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Escriba su correo electronico'" placeholder="Email">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control valid" name="phone" id="phone" type="number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Escriba un telefono de contacto'" placeholder="Teléfono">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control valid" name="location" id="location" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Escriba su estado y ciudad'" placeholder="Ciudad y estado">
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <button type="submit" class="button button-contactForm boxed-btn">Enviar</button>
                    </div>
                </form>
            </div>
            <div class="col-lg-3 offset-lg-1">
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-home"></i></span>
                    <div class="media-body">
                        <h3>Ciudad de México, CDMX</h3>
                        <p>Cerro Libertad 233, Campestre Churubusco, Coyoacán, 04200</p>
                    </div>
                </div>
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                    <div class="media-body">
                        <h3>+52 1 55 4085 4926</h3>
                        <p>Lunes a Sabado 9am to 6pm</p>
                    </div>
                </div>
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-email"></i></span>
                    <div class="media-body">
                        <h3>info@pmadvisor.mx</h3>
                        <p>Envíanos un mensaje desde cualquier dispositivo al email superior, estaremos encantados de resolver tus dudas.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ================ contact section end ================= -->

@stop