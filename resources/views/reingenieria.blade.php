@extends('base.base')

@section('activeHome') active @stop

@section('content')

<!-- bradcam_area  -->
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <h3>Servicios</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ bradcam_area  -->

<div class="case_details_area">
    <div class="container">
        <div class="border_bottom">
            <div class="row ">
                <div class="col-xl-12">
                    <div class="details_title">
                        <span>Reingenieria de procesos de negocio</span>
                        <h3>Desarrollo y estrategias corporativas para tu empresa.</h3>
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="case_thumb">
                        <img src="img/case/services-3.png" alt="Reingenieria de procesos de negocio">
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="details_main_wrap">
                        <p class="details_info">
                            Acompañamos a nuestros clientes en el desarrollo de planes y estrategias corporativas asegurando la identificación, implementación y mejora de sus procesos de negocio dentro de una cadena de valor robusta, logrando eficiencia a través del uso de tecnologías de información que permitan la adaptabilidad, automatización, agilidad y flexibilidad en la administración de los procesos significativos
                        </p>
                        <div class="single_details">
                            <ul class="list-group list-group-horizontal-sm">
                                <li class="list-group-item list-group-item-primary">Diagnósticos y arquitectura de procesos</li>
                                <li class="list-group-item list-group-item-primary">Reingeniería de procesos</li>
                                <li class="list-group-item list-group-item-primary">Definición de roles y responsabilidades</li>
                                <li class="list-group-item list-group-item-primary">Diseño, monitoreo y control (KPIs)</li>
                                <li class="list-group-item list-group-item-primary">Implementación de sistema de gestión de calidad</li>
                                <li class="list-group-item list-group-item-primary">Certificación ISO y control de riesgos</li>
                            </ul>
                        </div>
                        <p class="details_info">Atendemos de raíz la problemática de nuestros clientes conformando una solución con equipos multidisciplinarios. Nos enfocamos en la mejora del desempeño, transparencia y rentabilidad utilizando herramientas, metodologías y buenas prácticas de mejora continua.</p>
                        <div class="single_details">
                            <span>Resumen Reingenieria de procesos de negocio</span>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Análisis y Diseño detallado de requerimientos</li>
                                <li class="list-group-item">Implementación de proyectos – Lean, SixSigma</li>
                                <li class="list-group-item">Aplicación de metodologías tradicionales y ágiles</li>
                                <li class="list-group-item">Integración de Soluciones</li>
                                <li class="list-group-item">Diseño, Monitoreo y Control de Indicadores</li>
                                <li class="list-group-item">Implementación de Sistemas de Calidad</li>
                            </ul>
                        </div>
                        <div class="single_details mb-60">
                            <ul>
                                <li>
                                    <a href="/contacto"> <i class="fa fa-comment"> </i> Contactanos </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Information_area  -->
<div class="Information_area overlay">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-xl-8">
                <div class="info_text text-center">
                    <h3>Para dudas o aclaraciones puede contactarnos.</h3>
                    <p>Nuestros especialistas estarán encantados de atender su solicitud.</p>
                    <a class="boxed-btn3" href="https://wa.me/5540854926/?text=Me%20gustaria%20recibir%20más%20información" target="_blank">+52 1 55 4085 4926</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Information_area  end -->

@stop