@extends('base.base')

@section('activeHome') active @stop

@section('content')
<meta name=”robots” content=”noindex, follow”>

<!-- bradcam_area  -->
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <h3>Nuestros servicios</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ bradcam_area  -->

<!-- service_area_start -->
<div class="service_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center mb-50">
                    <h3>Que es lo que hacemos?</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/diagnostico.svg" width="40px" alt="Diagnóstico Empresarial">
                    </div>
                    <h3>Diagnóstico Empresarial</h3>
                    <p>Conocer la situación actual de la empresa nos permite la detección oportuna de problemas.</p>
                    <a href="/diagnosticoempresarial" class="learn_more">Leer más</a>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/idea.svg" width="40px" alt="Consultoría y administración de proyectos">
                    </div>
                    <h3>Consultoría y administración de proyectos</h3>
                    <p>Dirección de proyectos, de estructuras, planes y estrategias que nos permitan tener un control sobre: presupuestos, costos, tiempo, etc.</p>
                    <a href="/direcciondeproyectos" class="learn_more">Leer más</a>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/habilidad.svg" width="40px" alt="Reingenieria de procesos de negocio">
                    </div>
                    <h3>Reingenieria de procesos de negocio</h3>
                    <p>Acompañamos a nuestros clientes en el desarrollo de planes y estrategias corporativas.</p>
                    <a href="/reingenieriadeprocesos" class="learn_more">Leer más</a>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/cerebro.svg" width="40px" alt="Soluciones Tecnológicas">
                    </div>
                    <h3>Soluciones Tecnológicas</h3>
                    <p>Las mejores soluciones tecnologicas para implementar en tu empresa.</p>
                    <a href="/solucionestecnologicas" class="learn_more">Leer más</a>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/recursos-humanos.svg" width="40px" alt="Capital Humano">
                    </div>
                    <h3>Capital Humano</h3>
                    <p>Identificar las necesidades de nuestros colaboradores nos permite conocer las necesidades que nos impiden que realicen su trabajo satisfactoriamente.</p>
                    <a href="/capitalhumano" class="learn_more">Leer más</a>
                </div>
            </div>
            <!-- <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_service text-center">
                    <div class="service_icon">
                        <img src="img/svg_icon/6.svg" alt="">
                    </div>
                    <h3>Travel Agency </h3>
                    <p>Esteem spirit temper too say adieus who direct esteem.</p>
                    <a href="#" class="learn_more">Learn More</a>
                </div>
            </div> -->
        </div>
    </div>
</div>
<!-- service_area_end -->

<div class="contact_form_quote">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-lg-6">
                <div class="form_wrap">
                    <h3>Contáctanos ¿Que esperas?</h3>
                    @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="alert-heading">Muchas gracias por enviar tu mensaje!</h4>
                        <p>Aww yeah, muy pronto uno de nuestros asesores se comunicará contigo para resolver cualquier duda o sugerencia.</p>
                        <hr>
                        <p class="mb-0">{{Session::get('message')}}.</p>
                    </div>
                    @endif
                    <form action="/servicios" method="POST">
                        @csrf
                        <input name="name" type="text" placeholder="Nombre completo">
                        <input name="email" type="email" placeholder="Email">
                        <input name="phone" type="number" placeholder="Telefono">
                        <input name="location" type="text" placeholder="Ciudad y estado">
                        <textarea name="message" id="" cols="30" rows="10" placeholder="Escribe tu duda o sugerencia"></textarea>
                        <button type="submit" class="boxed-btn3">Enviar</button>
                    </form>
                </div>
            </div>
            <div class="col-xl-6 offset-xl-1 col-lg-6">
                <div class="contact_thumb">
                    <img src="img/banner/contact_thumb.png" alt="contato pmadvisor">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Information_area  -->
<div class="Information_area overlay">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-xl-8">
                <div class="info_text text-center">
                    <h3>Para dudas o aclaraciones puede contactarnos.</h3>
                    <p>Nuestros especialistas estarán encantados de atender su solicitud.</p>
                    <a class="boxed-btn3" href="https://wa.me/5540854926/?text=Me%20gustaria%20recibir%20más%20información" target="_blank">+52 1 55 4085 4926</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Information_area  end -->
@stop