<?php

namespace App\Http\Controllers;

use App\Mail\MessageReceived;
use App\Mail\MessageSuscribe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class message extends Controller
{
    public function store(){
        $message = request()->validate([
            'name' => 'Required',
            'email' => 'Required|email',
            'phone' => 'Required',
            'location' => 'Required',
            'message' => 'Required'
        ]);

        Mail::to('info@pmadvisor.mx')->queue(new MessageReceived($message));

        return Redirect::to('/contacto')->with('message', 'Tu mensaje ha sido enviado correctamente');
    }

    public function index(){
        $message = request()->validate([
            'name' => 'Required',
            'email' => 'Required|email',
            'phone' => 'Required',
            'location' => 'Required',
            'message' => 'Required'
        ]);

        Mail::to('info@pmadvisor.mx')->queue(new MessageReceived($message));

        return Redirect::to('/')->with('message', 'Tu mensaje ha sido enviado correctamente');
    }

    public function services(){
        $message = request()->validate([
            'name' => 'Required',
            'email' => 'Required|email',
            'phone' => 'Required',
            'location' => 'Required',
            'message' => 'Required'
        ]);

        Mail::to('info@pmadvisor.mx')->queue(new MessageReceived($message));

        return Redirect::to('/servicios')->with('message', 'Tu mensaje ha sido enviado correctamente');
    }

    public function suscribe(){
        $message = request()->validate([
            'email' => 'Required|email'
        ]);

        Mail::to('info@pmadvisor.mx')->queue(new MessageSuscribe($message));

        return Redirect::to('/')->with('message', 'se ha suscrito');
    }
}
