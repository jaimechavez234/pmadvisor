<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name("index");

Route::get('/nosotros', function () {
    return view('about');
})->name("nosotros");

Route::get('/contacto', function () {
    return view('contact');
})->name("contacto");

Route::get('/servicios', function () {
    return view('services');
})->name("servicios");

Route::get('/diagnosticoempresarial', function () {
    return view('diagnostico');
})->name("diagnosticoEmpresarial");

Route::get('/direcciondeproyectos', function () {
    return view('direccion');
})->name("direccionProyectos");

Route::get('/reingenieriadeprocesos', function () {
    return view('reingenieria');
})->name("reingenieriaProcesos");

Route::get('/capitalhumano', function () {
    return view('capital');
})->name("capitalHumano");

Route::get('/solucionestecnologicas', function () {
    return view('soluciones');
})->name("solucionestecnologicas");

Route::post('/contacto', 'message@store')->name('contacto');

Route::post('/', 'message@index');

Route::post('/servicios', 'message@services');

Route::post('/suscribe', 'message@suscribe')->name('suscribete');
